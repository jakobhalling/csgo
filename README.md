# CSGO SETTINGS

## LAUNCH OPTIONS

-processheap -novid -nojoy -noforcemparms -noforcemaccel +exec core.cfg -tickrate 128 -threads 8 -freq 144 -refresh 144 -nod3d9ex +cl_forcepreload 1 -console +mat_queue_mode 2 -w 1280 -h 1024 -d3d9ex

- Set threads to the amount of cores in system
- Set freq and refresh to monitor refresh rate

## CONFIG

STEAMFOLDER\userdata\SOMENUMBER\730\local\cfg\config.cfg

- Main: `6321506`
- Smurf: `8646398`

## git sparse checkout

`git clone --sparse --no-checkout https://bitbucket.org/jakobhalling/csgo.git .`

`git sparse-checkout set cfg`

`git checkout master`

Will checkout only cfg folder

## VIBRANCEGUI

vibranceGui for digital vibrance (75-100)
https://vibrancegui.com/

Brug CSGO only version:
https://vibrancegui.com/vibrance/version1

## WINDOWS PERFORMANCE SETTINGS

- System -> Advanced System Settings -> Performance -> Settings

- Set to "Adjust for best performance" and reenable "Smooth edges of screen fonts"

- Additionally: Remeber to set windows power management to "High performance"

- Disable windows gaming features (recording, gamebar etc.) - clone repo - run \*.reg files.

## BENCHMARKING & HW DEBUGGING

- CPU-Z
- GPU-Z
- Windows ressource monitor
- procdump CPU dump: https://docs.microsoft.com/en-us/sysinternals/downloads/procdump (https://www.itprotoday.com/management-mobility/got-high-cpu-usage-problems-procdump-em)
- ATTO Disk benchmark (HDD / SSD benchmark)

## Sound settings

```
dsp_enhance_stereo "0";
snd_mixahead "0.02";
snd_pitchquality "1";
snd_deathcamera_volume "0.0";
snd_mapobjective_volume "0.12";
snd_menumusic_volume "0.0";
snd_mvp_volume "0.11";
snd_roundend_volume "0.2";
snd_roundstart_volume "0.06";
snd_tensecondwarning_volume "0.35";
voice_scale "0.62";
snd_mute_losefocus "0";
snd_surround_speakers "0"
```

## EXPERIMENTAL
### Chris Citus Tech - debloater software
https://christitus.com/debloat-windows-10-2020/
See entire windows gaming optimization video:

### ADDITIONAL MINOR TWEAKS

- Set Windows power options to run "High Performance"
  \*\* Control Panel\Hardware and Sound\Power Options

### CORE UNPARKING

Disable Windows Core parking:
http://coderbag.com/Programming-C/Disable-CPU-Core-Parking-Utility

### MSISupported (HIGH FPS BOOST ON SOME SYSTEMS)

- Go to Device manager -> Display Adapter -> gfx card -> properties -> Details -> Choose Hardware Ids -> See the first value
- Go to HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Enum\PCI\***\*\***\***\*\*\*\***\Device Parameters\Interrupt Management\MessageSignaledInterruptProperties where stars are device specific information, the first being the hardware id
- Add a new folder called "MessageSignaledInterruptProperties"
- Add a DWORD 32 bit value named MSISupported and set value to 1
- restart pc

### Nvidia Inspector

http://www.guru3d.com/files-details/nvidia-inspector-download.html

- Download above program
- Launch it
- Click "Add application to current profile"
- Find csgo.exe
- Click "Apply Changes"

## DRIVER UPDATES
